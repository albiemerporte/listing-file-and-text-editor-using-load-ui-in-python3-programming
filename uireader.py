from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import *
import subprocess
import os


app = QtWidgets.QApplication([])
mainui = uic.loadUi('textwriter.ui')
createfileui = uic.loadUi('creatingtextfile.ui')

def disableornot():
    toopenfile = mainui.lblcurrentfileshow.text()
    if toopenfile == 'the file':
        mainui.pbsave.setEnabled(False)
    else:
        mainui.pbsave.setEnabled(True)

def filels():
    # Execute the command in the terminal and capture its output
    command_result = subprocess.check_output(['ls'], shell=True)

    

    # Decode the byte string output and print it
    mainui.txtlistfile.setText(command_result.decode('utf-8'))

mainui.txtEditwrite.setText("")
def filenew():
    createfileui.txtfilename.clear()
    createfileui.txtfilename.setFocus()
    createfileui.show()

def savefile():
    contents = mainui.txtEditwrite.toPlainText()
    toopenfile = mainui.lblcurrentfileshow.text()
    
    file = open(toopenfile, "w")
    file.write(contents)
    print(toopenfile)
    file.close()
    
    mainui.lblcurrentfileshow.setText("saved")
    mainui.pbsave.setEnabled(False)
    
def createfilesave():
    myfile = createfileui.txtfilename.text()  
    
    if os.path.exists(myfile):
        file = open(myfile, 'r')
        contents = file.read()
        file.close()
    else:
        os.system("touch {}".format(myfile))
        file = open(myfile, 'r')
        contents = file.read()
        file.close()
        
    mainui.txtEditwrite.setPlainText(contents)
    mainui.txtEditwrite.setFocus()
    mainui.lblcurrentfileshow.setText(myfile)
    
    command_result = subprocess.check_output(['ls'], shell=True)
    mainui.txtlistfile.setText(command_result.decode('utf-8'))
    
    createfileui.close()
    
    disableornot()
    
def createfilecancel():
    createfileui.close()

mainui.pbpwd.clicked.connect(filels)
mainui.pbnew.clicked.connect(filenew)
mainui.pbsave.clicked.connect(savefile)

createfileui.pbcancel.clicked.connect(createfilecancel)
createfileui.pbsave.clicked.connect(createfilesave)

if __name__ == '__main__':
    mainui.show()
    disableornot()
    app.exec()